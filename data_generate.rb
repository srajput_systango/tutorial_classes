class DataGenerate

	def initialize(data)
		@data=data
	end

	def data_generate
		@price_array=Array.new
		@country_array=Array.new
		$i=0
		$limit=@data.length

		@price_column=0
		@country_column=1

		while $i<$limit do

			if /^(?<num>\d+)$/ =~ @data[$i][@price_column]			#to check whether the given string is numeric or not
				@price_array[$i]=@data[$i][@price_column].to_i
			else
				@price_array[$i]="Bad Data"
			end
			
			if /^(?<num>\d+)$/ =~ @data[$i][@country_column] or  @data[$i][@country_column]==nil  #to check whether the given string is word or not
				@country_array[$i]="Bad Data"
			else
				@country_array[$i]=@data[$i][@country_column]
			end

			$i+=1
		end
	end

	def data_price
		return @price_array
	end

	def data_country
		return @country_array
	end

	def data_number
		return @data.length
	end
end

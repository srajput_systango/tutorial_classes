class OutputWriter
	def initialize(output_file_name,modified_array)
		@output_file_name=output_file_name
		@modified_array=modified_array
	end

	def data_write
		CSV.open(@output_file_name,"w+b") do |csv|
			csv<<["Price","Country","Sales Tax"]

			$i=0
			$limit=@modified_array.length
			
			while $i<$limit do
				csv<<@modified_array[$i]

				$i+=1
			end
		end
	end
end

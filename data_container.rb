class DataContainer
	def initialize(price,country)
		@price_storage=price
		@country_storage=country
	end

	def data_required_price(i)
		return @price_storage[i]
	end

	def data_required_country(i)
		return @country_storage[i]
	end
end
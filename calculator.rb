class Calculator
	def initialize(price,country)
		@price_temporary=price
		@country_temporary=country
	end

	def calculate
		if @country_temporary==nil
			sales_tax=0
		elsif @country_temporary=="Bad Data"
			sales_tax="Sales Tax can't be Computed, Bad Data"
		elsif @country_temporary.casecmp("India")==0
 			sales_tax=india_tax_slab()
  		elsif @country_temporary.casecmp("USA")==0
  			sales_tax=usa_tax_slab()
  		elsif @country_temporary.casecmp("UK")==0
  			sales_tax=uk_tax_slab()
  		else
  			sales_tax=other_tax_slab()
  		end
		return sales_tax
	end

	def india_tax_slab
		if @price_temporary=="Bad Data"
			sales_tax="Sales Tax can't be Computed, Bad Data"
		elsif @price_temporary<=100
  			sales_tax=0
  		elsif @price_temporary>100 && @price_temporary<=500
     		sales_tax= (@price_temporary-100)* 0.05
  		elsif @price_temporary>500
  			sales_tax= (@price_temporary-500)*0.2+ 400*0.05
  		end
		return sales_tax
	end

	def usa_tax_slab
		if @price_temporary=="Bad Data"
			sales_tax="Sales Tax can't be Computed, Bad Data"
		else
			sales_tax=Math.sqrt(@price_temporary)
		end
		return sales_tax
	end

	def uk_tax_slab
		if @price_temporary=="Bad Data"
			sales_tax="Sales Tax can't be Computed, Bad Data"
		else
			sales_tax=(@price_temporary)*0.03
		end
		return sales_tax
	end

	def other_tax_slab
		if @price_temporary=="Bad Data"
			sales_tax="Sales Tax can't be Computed, Bad Data"
		else
			sales_tax=" Program doesn't Support Your Country "
		end
		return sales_tax
	end
end
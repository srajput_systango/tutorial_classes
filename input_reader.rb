require 'csv'

class InputReader
	
	def initialize(file_name)
		@file_name=file_name
	end

	def check_file
		if File.exists?(@file_name)
			return true
		else
			puts "File doesn't exists,Program is Terminating"
			exit
		end
	end

	def data_extract_file
		@data=CSV.read(@file_name,headers:true)
		return @data
	end
end
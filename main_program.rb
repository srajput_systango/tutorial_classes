require_relative 'input_reader'
require_relative 'data_generate'
require_relative 'data_container'
require_relative 'calculator'
require_relative 'item'
require_relative 'output_writer'


read_data=InputReader.new("input.csv")
check=read_data.check_file
data_array=read_data.data_extract_file


data_separate=DataGenerate.new(data_array)
data_separate.data_generate()
price_array=data_separate.data_price()
country_array=data_separate.data_country()
$limit=data_separate.data_number()


data_storage=DataContainer.new(price_array,country_array)

$i=0

while $i<$limit do
	
	item_number=$i

	temporary_price=data_storage.data_required_price(item_number)
	temporary_country=data_storage.data_required_country(item_number)

	item=Item.new(temporary_price,temporary_country)
	item.process()
	item.data_to_array()

	$i+=1
end

array_to_write=item.modified_array()

data_writer=OutputWriter.new("output_class_modified.csv",array_to_write)
data_writer.data_write()
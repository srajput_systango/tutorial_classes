class Item
	
	@@count=0
	@@new_data_array=Array.new

	def initialize(price,country)
		@price=price
		@country=country
	end

	def process
		h=Calculator.new(@price,@country)
		@sales_tax=h.calculate()
	end

	def data_to_array
		@@new_data_array[@@count]=[@price,@country,@sales_tax]
		
		@@count+=1
	end

	def modified_array
		return @@new_data_array
	end
end
